'use strict';

/**
 * @ngdoc overview
 * @name carracingApp
 * @description
 * # carracingApp
 *
 * Main module of the application.
 */
angular
  .module('carracingApp', [
    'game',
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .controller('GameController', function(GameManager) { 
    this.game = GameManager; 
  });