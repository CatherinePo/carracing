'use strict';

/**
 * @ngdoc function
 * @name carracingApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the carracingApp
 */
angular.module('carracingApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
